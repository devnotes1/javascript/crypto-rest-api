import '../scss/style.scss';

const container = document.querySelector('.container__crypto');
const search = document.querySelector('.container-search');
const input = document.querySelector('input');
const btn = document.querySelector('button');
const single = document.querySelector('.single');
const cross = document.querySelector('.cross');

const singleImg = document.querySelector('.single__img');

const options = {
	method: 'GET',
	headers: { accept: 'application/json', 'x-cg-demo-api-key': 'CG-Wcsj9F9y8k9W8kAvnRD4qjfc' },
};

fetch('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd', options)
	.then(response => response.json())
	.then(res => {
		console.log(res);
		for (let coin of res) {
			btn.addEventListener('click', e => {
				e.preventDefault();
				const inputValue = input.value;
				search.style.top = '0px';
				single.classList.add('open');
				cross.classList.add('show');

				if (coin.name.toLowerCase() === inputValue.toLowerCase()) {
					single.innerHTML = '';
					const price = document.createElement('div');
					const priceNumber = document.createElement('div');
					const priceChange = document.createElement('div');
					const image = document.createElement('img');
					const img = document.createElement('div');
					const info = document.createElement('div');

					price.classList.add('price');
					priceNumber.classList.add('price__number');
					priceChange.classList.add('price__change');
					img.classList.add('single__image');
					image.classList.add('single__img');
					info.classList.add('info');

					priceNumber.textContent = '$' + coin.current_price.toLocaleString('en-US');
					priceChange.textContent = Math.round(coin.price_change_percentage_24h * 10) / 10 + '%';
					image.setAttribute('src', coin.image);

					single.appendChild(price);
					single.appendChild(img);
					single.appendChild(info);
					price.appendChild(priceNumber);
					price.appendChild(priceChange);
					img.appendChild(image);

					for (let key in coin) {

						if (key === 'market_cap_rank' || key === 'circulating_supply' || key === 'max_supply') {
							
							const div = document.createElement('div');
							div.classList.add('item');
							div.innerHTML = `<div class="heading">${key}</div><div class="content">${coin[key]}</div>`;
							info.appendChild(div);

						} else if (key === 'low_24h' || key === 'high_24h' || key === 'market_cap' || key === 'ath') {

							let coinDolar = coin[key].toLocaleString('en-US');
							const div = document.createElement('div');
							div.classList.add('item');
							div.innerHTML = `<div class="heading">${key}</div><div class="content">${coinDolar}/USD</div>`;
							info.appendChild(div);

						} else if (key === 'ath_change_percentage') {
							
							const div = document.createElement('div');
							div.classList.add('item');
							div.innerHTML = `<div class="heading">${key}</div><div class="content">${coin[key]}%</div>`;
							info.appendChild(div);

						}
						
					}

					
					// const marketCapLocal = coin.market_cap.toLocaleString('en-US');

					// const marketRank = document.createElement('div');
					// const marketCap = document.createElement('div');
					// const fullyValuation = document.createElement('div');
					// const totalSupply = document.createElement('div');
					// const maxSupply = document.createElement('div');

					// marketRank.classList.add('item');
					// marketCap.classList.add('item');
					// fullyValuation.classList.add('item');
					// totalSupply.classList.add('item');
					// maxSupply.classList.add('item');

					// marketRank.innerHTML = `<div class="heading">Market Cap Rank</div><div class="content">#${coin.market_cap_rank
					// }</div>`;
					// marketCap.innerHTML = `<div class="heading">Market Cap</div><div class="content">$${marketCapLocal}</div>`;
					// fullyValuation.innerHTML = `<div class="heading">Fully Diluted Valuation</div><div class="content">$${coin.fully_diluted_valuation}</div>`;
					// totalSupply.innerHTML = `<div class="heading">Total Supply</div><div class="content">${coin.total_supply}</div>`;
					// maxSupply.innerHTML = `<div class="heading">Max Supply</div><div class="content">${coin.max_supply}</div>`;

					// info.appendChild(marketRank);
					// info.appendChild(marketCap);
					// info.appendChild(fullyValuation);
					// info.appendChild(totalSupply);
				}
			});
		}
		for (let i = 0; i < 5; i++) {
			let coinName = res[i].name;
			let coinPrice = res[i].current_price;
			let coinUrl = res[i].image;
			let priceChange = res[i].price_change_percentage_24h;

			const div = document.createElement('div');
			div.classList = 'coin';
			const contImg = document.createElement('div');
			const img = document.createElement('img');
			img.setAttribute('src', coinUrl);
			img.classList = 'img';
			const name = document.createElement('div');
			name.textContent = coinName;
			const price = document.createElement('div');
			price.textContent = `${coinPrice}/USD`;
			if (priceChange >= 0) {
				price.classList.add('green');
			} else {
				price.classList.add('red');
			}
			container.appendChild(div);
			div.appendChild(contImg);
			div.appendChild(name);
			div.appendChild(price);
			contImg.appendChild(img);

			
		}
	});
///wyszukiwarka

cross.addEventListener('click', () => {
	input.value = '';
	single.innerHTML = '';
	search.style.top = '50px';
	single.classList.remove('open');
	cross.classList.remove('show');
});
